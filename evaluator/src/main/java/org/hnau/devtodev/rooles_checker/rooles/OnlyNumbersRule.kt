package org.hnau.devtodev.rooles_checker.rooles

import org.hnau.devtodev.R
import org.hnau.devtodev.base.check.CardNumberCheckingException
import org.hnau.devtodev.utils.StringGetter


/**
 * CardNumberCheckingRule that checks, that card number contains only numbers
 */
class OnlyNumbersRule : CardNumberCheckingRule {

    /**
     * CardNumberCheckingException for card with no digit symbol in number
     * @param cardNumber - Incorrect number of card
     * @param notDigitChar - Not digit char of card number
     */
    class Exception(
            cardNumber: String,
            notDigitChar: Char
    ) : CardNumberCheckingException(
            cardNumber = cardNumber,
            text = StringGetter(R.string.only_numbers_rule_reason, notDigitChar)
    )

    /**
     * throws OnlyNumbersRule.Exception if cardNumber contains not digit char
     * @param cardNumber Card number to check
     * @return if cardNumber is correct
     */
    override fun check(
            cardNumber: String
    ) {
        cardNumber
                .find { !it.isDigit() }
                ?.let { throw Exception(cardNumber, it) }
    }
}