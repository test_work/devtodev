package org.hnau.devtodev.rooles_checker.rooles

import org.hnau.devtodev.R
import org.hnau.devtodev.base.check.CardNumberCheckingException
import org.hnau.devtodev.utils.StringGetter


/**
 * CardNumberCheckingRule that checks, that card number length in 12..19
 */
class From12To19SymbolsRule : CardNumberCheckingRule {

    companion object {

        //Minimum length of correct card number
        private const val MIN_LENGTH = 12

        //Maximum length of correct card number
        private const val MAX_LENGTH = 19

        //Correct card number length range
        private val CORRECT_LENGTH_RANGE = MIN_LENGTH..MAX_LENGTH

    }

    /**
     * CardNumberCheckingException for card with number with incorrect length
     * @param cardNumber - Incorrect number of card
     * @param cardNumberLength - Length of card number
     */
    class Exception(
            cardNumber: String,
            cardNumberLength: Int
    ) : CardNumberCheckingException(
            cardNumber = cardNumber,
            text = StringGetter(R.string.from_12_to_19_length_rule_reason, cardNumberLength, MIN_LENGTH, MAX_LENGTH)
    )

    /**
     * throws From12To19SymbolsRule.Exception if cardNumber length not in [CORRECT_LENGTH_RANGE]
     * @param cardNumber Card number to check
     * @return if cardNumber is correct
     */
    override fun check(
            cardNumber: String
    ) {
        val length = cardNumber.length
        if (length !in CORRECT_LENGTH_RANGE) {
            throw Exception(cardNumber, length)
        }
    }
}