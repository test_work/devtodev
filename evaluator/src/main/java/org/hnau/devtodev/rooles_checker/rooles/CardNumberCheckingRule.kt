package org.hnau.devtodev.rooles_checker.rooles

import org.hnau.devtodev.base.check.CardNumberCheckingException

/**
 * Rule to check card number
 */
interface CardNumberCheckingRule {

    @Throws(CardNumberCheckingException::class)
    fun check(cardNumber: String)

}