package org.hnau.devtodev.rooles_checker

import org.hnau.devtodev.base.check.CardNumberChecker
import org.hnau.devtodev.rooles_checker.rooles.CardNumberCheckingRule


/**
 * CardNumberChecker based on rules
 * @property rules - rules to check card number
 */
open class RulesCardNumberChecker(
        private val rules: Iterable<CardNumberCheckingRule>
) : CardNumberChecker {

    override fun checkCardNumber(cardNumber: String) =
            rules.forEach { rule -> rule.check(cardNumber) }

}