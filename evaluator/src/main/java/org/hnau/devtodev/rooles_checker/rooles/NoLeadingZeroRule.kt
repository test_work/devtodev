package org.hnau.devtodev.rooles_checker.rooles

import org.hnau.devtodev.R
import org.hnau.devtodev.base.check.CardNumberCheckingException
import org.hnau.devtodev.utils.StringGetter


/**
 * CardNumberCheckingRule that checks, that card number not starts with zero
 */
class NoLeadingZeroRule : CardNumberCheckingRule {

    /**
     * CardNumberCheckingException for card with leading zero
     * @param cardNumber - Incorrect number of card
     */
    class Exception(
            cardNumber: String
    ) : CardNumberCheckingException(
            cardNumber = cardNumber,
            text = StringGetter(R.string.no_leading_zero_rule_reason)
    )

    /**
     * throws NoLeadingZeroRule.Exception if cardNumber starts with zero
     * @param cardNumber Card number to check
     * @return if cardNumber is correct
     */
    override fun check(
            cardNumber: String
    ) {
        if (cardNumber.startsWith("0")) {
            throw Exception(cardNumber)
        }
    }
}