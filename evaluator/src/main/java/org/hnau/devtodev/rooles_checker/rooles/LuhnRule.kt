package org.hnau.devtodev.rooles_checker.rooles

import org.hnau.devtodev.R
import org.hnau.devtodev.base.check.CardNumberCheckingException
import org.hnau.devtodev.utils.StringGetter


/**
 * CardNumberCheckingRule that checks Luhn chechsum
 */
class LuhnRule : CardNumberCheckingRule {

    /**
     * CardNumberCheckingException for card whitch number not pass Luhn chechsum algorythm
     * @param cardNumber - Incorrect number of card
     */
    class Exception(
            cardNumber: String
    ) : CardNumberCheckingException(
            cardNumber = cardNumber,
            text = StringGetter(R.string.luhn_rule_reason)
    )

    /**
     * throws LuhnRule.Exception if cardNumber not pass Luhn chechsum algorythm
     * @param cardNumber Card number to check
     * @return if cardNumber is correct
     */
    override fun check(
            cardNumber: String
    ) {
        if (!checkLuhn(cardNumber)) {
            throw Exception(cardNumber)
        }
    }

    private fun checkLuhn(value: String) =
            value.toCharArray()
                    .map { it - '0' }
                    .filter { it in 0..9 }
                    .reversed()
                    .asSequence()
                    .mapIndexed { index, item ->
                        when {
                            index % 2 == 1 -> (item * 2).let { if (it > 9) it - 9 else it }
                            else -> item
                        }
                    }.sum() % 10 == 0

}