package org.hnau.devtodev

import org.hnau.devtodev.rooles_checker.RulesCardNumberChecker
import org.hnau.devtodev.rooles_checker.rooles.From12To19SymbolsRule
import org.hnau.devtodev.rooles_checker.rooles.LuhnRule
import org.hnau.devtodev.rooles_checker.rooles.NoLeadingZeroRule
import org.hnau.devtodev.rooles_checker.rooles.OnlyNumbersRule

/**
 * RulesCardNumberChecker that uses rules: OnlyNumbersRule, NoLeadingZeroRule, From12To19SymbolsRule, LuhnRule
 */
class SimpleCardNumberChecker : RulesCardNumberChecker(
        rules = RULES
) {

    companion object {

        private val RULES = listOf(
                OnlyNumbersRule(),
                NoLeadingZeroRule(),
                From12To19SymbolsRule(),
                LuhnRule()
        )

    }

}