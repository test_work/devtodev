package org.hnau.devtodev

import com.google.gson.Gson
import org.hnau.devtodev.base.info.CardInfoResolver
import org.hnau.devtodev.base.info.entity.CardInfo
import java.io.BufferedInputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLConnection
import java.util.concurrent.Executor
import kotlin.concurrent.thread
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

/**
 * CardInfoResolver that gets information about card based on https://binlist.net/
 */
class BinlistCardInfoResolver(
        private val executor: Executor
) : CardInfoResolver {

    companion object {

        private const val BINLIST_URL_PREFIX = "https://lookup.binlist.net/"
        private val HEADERS = listOf("Accept-Version" to "3")

    }

    /**
     * get info about card with given number
     */
    override suspend fun getCardInfo(
            cardNumber: String
    ) = suspendCoroutine<CardInfo> { continuation ->
        executor.execute {
            try {
                val cardInfo = getCardInfoSync(cardNumber)
                continuation.resume(cardInfo)
            } catch (th: Throwable) {
                continuation.resumeWithException(th)
            }
        }
    }

    private fun getCardInfoSync(cardNumber: String): CardInfo {
        val cardInfoJson = loadCardInfoJson(cardNumber)
        return Gson().fromJson(cardInfoJson, CardInfo::class.java)
    }

    private fun loadCardInfoJson(cardNumber: String): String {
        val connection = prepareConnection(cardNumber)
        return try {
            String(connection.inputStream.readBytes())
        } finally {
            connection.disconnect()
        }
    }

    private fun prepareConnection(cardNumber: String): HttpURLConnection {
        val url = createUrl(cardNumber)
        val connection = url.openConnection() as HttpURLConnection
        HEADERS.forEach { (key, value) ->
            connection.setRequestProperty(key, value)
        }
        return connection
    }

    private fun createUrl(cardNumber: String) =
            URL(BINLIST_URL_PREFIX + cardNumber)

}