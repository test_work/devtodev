package org.hnau.devtodev.base

import org.hnau.devtodev.base.check.CardNumberChecker
import org.hnau.devtodev.base.check.CardNumberCheckingException
import org.hnau.devtodev.base.info.CardInfoResolver
import org.hnau.devtodev.base.info.entity.CardInfo

/**
 * @property cardNumberChecker Class that can verify the correctness of a card number
 * @property cardInfoResolver Class that can get information about card
 */
open class CardNumberEvaluator(
        private val cardNumberChecker: CardNumberChecker,
        private val cardInfoResolver: CardInfoResolver
) {

    /**
     * Verify the correctness of a card number and get information about this card
     * @param cardNumber number of the card
     * @return information about card
     * @throws CardNumberCheckingException if card number is incorrect
     */
    @Throws(CardNumberCheckingException::class)
    suspend fun evaluate(cardNumber: String): CardInfo {
        cardNumberChecker.checkCardNumber(cardNumber)
        return cardInfoResolver.getCardInfo(cardNumber)
    }

}