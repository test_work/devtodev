package org.hnau.devtodev.base.info.entity.additional

import org.hnau.devtodev.R
import org.hnau.devtodev.utils.StringGetter

/**
 * Card payment system
 * @param title visible to user title of this type
 */
enum class Scheme(
    val title: StringGetter
) {
    amex(StringGetter(R.string.card_info_scheme_title_amex)),
    diners(StringGetter(R.string.card_info_scheme_title_diners)),
    visa(StringGetter(R.string.card_info_scheme_title_visa)),
    mastercard(StringGetter(R.string.card_info_scheme_title_mastercard)),
    discover(StringGetter(R.string.card_info_scheme_title_discover)),
    unionpay(StringGetter(R.string.card_info_scheme_title_unionpay)),
    jcb(StringGetter(R.string.card_info_scheme_title_jcb))
}