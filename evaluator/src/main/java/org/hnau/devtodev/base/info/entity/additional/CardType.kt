package org.hnau.devtodev.base.info.entity.additional

import org.hnau.devtodev.R
import org.hnau.devtodev.utils.StringGetter

/**
 * Type of card (debit or credit)
 * @param title visible to user title of this type
 */
enum class CardType(
    val title: StringGetter
) {

    debit(StringGetter(R.string.card_info_type_title_debit)),
    credit(StringGetter(R.string.card_info_type_title_credit))

}