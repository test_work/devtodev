package org.hnau.devtodev.base.info.entity.additional

/**
 * Country of card
 * @property name Country name
 * @property emoji Country flag
 */
data class Country(
        val name: String,
        val emoji: String
)