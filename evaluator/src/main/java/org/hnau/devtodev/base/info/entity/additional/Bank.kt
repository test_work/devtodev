package org.hnau.devtodev.base.info.entity.additional

/**
 *
 * @property name Bank name
 * @property url Bank url (if exists)
 * @property phone Bank phone (if exists)
 * @property city Bank city (if exists)
 * @constructor
 */
data class Bank(
        val name: String,
        val url: String?,
        val phone: String?,
        val city: String?
)