package org.hnau.devtodev.base.info.entity

import org.hnau.devtodev.base.info.entity.additional.Bank
import org.hnau.devtodev.base.info.entity.additional.CardType
import org.hnau.devtodev.base.info.entity.additional.Country
import org.hnau.devtodev.base.info.entity.additional.Scheme

/**
 * Info about card to show to user
 */
data class CardInfo(
        val type: CardType,
        val scheme: Scheme,
        val country: Country,
        val bank: Bank
)