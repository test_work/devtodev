package org.hnau.devtodev.base.info

import org.hnau.devtodev.base.info.entity.CardInfo

/**
 * Class that can get information about card
 */
interface CardInfoResolver {

    /**
     * get info about card with given number
     */
    suspend fun getCardInfo(cardNumber: String): CardInfo

}