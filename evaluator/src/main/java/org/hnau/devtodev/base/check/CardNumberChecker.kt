package org.hnau.devtodev.base.check

/**
 * Class that can verify the correctness of a card number
 */
interface CardNumberChecker {

    @Throws(CardNumberCheckingException::class)
    fun checkCardNumber(cardNumber: String)

}