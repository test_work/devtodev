package org.hnau.devtodev.base.check

import org.hnau.devtodev.utils.StringGetter
import java.lang.IllegalArgumentException

/**
 * Base card number checking exception
 * @param cardNumber Incorrect number of card
 * @param text Reason
 */
abstract class CardNumberCheckingException(
        val cardNumber: String,
        val text: StringGetter
) : IllegalArgumentException(
        "Card number is incorrect"
)