package org.hnau.devtodev.utils

import android.content.Context

/**
 * Resolver of string by context
 * @property resolver resolver of the String by Context
 */
class StringGetter(
        private val resolver: (Context) -> String
) : (Context) -> String {

    /**
     * Create StringGetter fron resources
     * @param resId resources idenifier
     * @param params additional values
     */
    constructor(
            resId: Int,
            vararg params: Any
    ) : this(
            resolver = { context ->
                context.getString(resId, *params)
            }
    )

    private var cachedString: String? = null

    override fun invoke(
            context: Context
    ): String = synchronized(this) {
        var result = cachedString
        if (result == null) {
            result = resolver(context)
            cachedString = result
        }
        return@synchronized result
    }

}