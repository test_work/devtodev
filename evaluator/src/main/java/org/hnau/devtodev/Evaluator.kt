package org.hnau.devtodev

import org.hnau.devtodev.base.CardNumberEvaluator
import org.hnau.devtodev.base.check.CardNumberChecker
import org.hnau.devtodev.base.info.CardInfoResolver
import java.util.concurrent.Executor

/**
 * CardNumberEvaluator that uses SimpleCardNumberChecker as cardNumberChecker and BinlistCardInfoResolver as cardInfoResolver
 * @param executor Executor to execute BinlistCardInfoResolver.getCardInfo
 */
class Evaluator(
        executor: Executor
) : CardNumberEvaluator(
        cardNumberChecker = SimpleCardNumberChecker(),
        cardInfoResolver = BinlistCardInfoResolver(executor)
)