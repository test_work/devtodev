package org.hnau.devtodev

import kotlinx.coroutines.runBlocking
import org.hnau.devtodev.base.info.entity.CardInfo
import org.hnau.devtodev.base.info.entity.additional.Bank
import org.hnau.devtodev.base.info.entity.additional.CardType
import org.hnau.devtodev.base.info.entity.additional.Country
import org.hnau.devtodev.base.info.entity.additional.Scheme
import org.junit.Test

import org.junit.Assert.*
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor

class BinlistCardInfoResolverTest {

    @Test
    fun getCardInfoTest() {

        val executor = Executors.newSingleThreadExecutor()
        val binlistCardInfoResolver = BinlistCardInfoResolver(executor)

        val variants = listOf(
                "42764900" to CardInfo(
                        type = CardType.debit,
                        scheme = Scheme.visa,
                        country = Country(
                                name = "Russian Federation",
                                emoji = "\uD83C\uDDF7\uD83C\uDDFA"
                        ),
                        bank = Bank(
                                name = "SAVINGS BANK OF THE RUSSIAN FEDERATION (SBERBANK)",
                                city = null,
                                phone = null,
                                url = "www.sbrf.ru"
                        )
                ),
                "45717360" to CardInfo(
                        type = CardType.debit,
                        scheme = Scheme.visa,
                        country = Country(
                                name = "Denmark",
                                emoji = "\uD83C\uDDE9\uD83C\uDDF0"
                        ),
                        bank = Bank(
                                name = "Jyske Bank",
                                city = "Hjørring",
                                phone = "+4589893300",
                                url = "www.jyskebank.dk"
                        )
                )
        )

        runBlocking {
            variants.forEach { (cardNumber, expected) ->
                val actual = binlistCardInfoResolver.getCardInfo(cardNumber)
                assertEquals(expected, actual)
            }
        }

    }
}