package org.hnau.devtodev.rooles_checker.rooles

import org.hnau.devtodev.base.check.CardNumberCheckingException
import org.junit.Assert.*
import org.junit.Test

class CardNumberCheckingRulesTest {

    @Test
    fun noLeadingZeroRuleTest() =
            testRule(
                    rule = NoLeadingZeroRule(),
                    exceptionClass = NoLeadingZeroRule.Exception::class.java,
                    variants = listOf(
                            "" isCorrect true,
                            "0" isCorrect false,
                            "00" isCorrect false,
                            "001" isCorrect false,
                            "1" isCorrect true,
                            "10" isCorrect true
                    )
            )

    @Test
    fun from12To19SymbolsRuleTest() =
            testRule(
                    rule = From12To19SymbolsRule(),
                    exceptionClass = From12To19SymbolsRule.Exception::class.java,
                    variants = listOf(
                            "" isCorrect false,
                            "0" isCorrect false,
                            "00000000" isCorrect false,
                            "000000000000" isCorrect true,
                            "0000000000000000" isCorrect true,
                            "0000000000000000000" isCorrect true,
                            "00000000000000000000" isCorrect false
                    )
            )

    @Test
    fun onlyNumbersRuleTest() =
            testRule(
                    rule = OnlyNumbersRule(),
                    exceptionClass = OnlyNumbersRule.Exception::class.java,
                    variants = listOf(
                            " " isCorrect false,
                            "a" isCorrect false,
                            "0 " isCorrect false,
                            " 0" isCorrect false,
                            "0" isCorrect true
                    )
            )

    @Test
    fun luhnRuleTest() =
            testRule(
                    rule = LuhnRule(),
                    exceptionClass = LuhnRule.Exception::class.java,
                    variants = listOf(
                            "" isCorrect true,
                            "0" isCorrect true,
                            "4276490018703548" isCorrect true,
                            "4276490018703549" isCorrect false
                    )
            )

    private data class RooleTestVariant(
            val cardNumber: String,
            val isCorrect: Boolean
    )

    private infix fun String.isCorrect(isCorrect: Boolean) =
            RooleTestVariant(this, isCorrect)

    private fun testRule(
            rule: CardNumberCheckingRule,
            variants: Iterable<RooleTestVariant>,
            exceptionClass: Class<out CardNumberCheckingException>
    ) = variants.forEach { (cardNumber, correct) ->
        val expected = if (correct) null else exceptionClass
        val actual = getThrowed { rule.check(cardNumber) }?.javaClass
        assertEquals(expected, actual)
    }

    private inline fun getThrowed(
            throwsAction: () -> Unit
    ) = try {
        throwsAction(); null
    } catch (th: Throwable) {
        th
    }

}