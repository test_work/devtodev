package org.hnau.test.evaluator_example.utils

import android.view.View
import android.widget.TextView
import org.hnau.devtodev.base.info.entity.CardInfo


/**
 * Aggregator of card info valuating result showing views
 */
class EvaluatingCardResultContainer(
        private val errorTextView: TextView,
        private val infoMainView: View,
        private val typeTextView: TextView,
        private val schemeTextView: TextView,
        private val countryNameTextView: TextView,
        private val countryFlagTextView: TextView,
        private val bankNameTextView: TextView,
        private val bankCityTextView: TextView,
        private val bankSiteTextView: TextView,
        private val bankPhoneTextView: TextView
) {

    private val cardInfoContainer = CardInfoContainer(
            infoMainView,
            typeTextView,
            schemeTextView,
            countryNameTextView,
            countryFlagTextView,
            bankNameTextView,
            bankCityTextView,
            bankSiteTextView,
            bankPhoneTextView
    )

    private val errorContainer = EvaluatingErrorContainer(
            errorTextView
    )

    //show card number evaluating success result
    fun onSuccess(cardInfo: CardInfo) {
        cardInfoContainer.cardInfo = cardInfo
        errorContainer.error = null

    }

    //show card number evaluating error result
    fun onError(error: Throwable) {
        cardInfoContainer.cardInfo = null
        errorContainer.error = error
    }

}