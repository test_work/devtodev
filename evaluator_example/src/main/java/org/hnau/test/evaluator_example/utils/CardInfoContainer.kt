package org.hnau.test.evaluator_example.utils

import android.content.Context
import android.view.View
import android.widget.TextView
import org.hnau.devtodev.base.info.entity.CardInfo


/**
 * Aggregator of card info showing views
 */
class CardInfoContainer(
        private val infoMainView: View,
        private val typeTextView: TextView,
        private val schemeTextView: TextView,
        private val countryNameTextView: TextView,
        private val countryFlagTextView: TextView,
        private val bankNameTextView: TextView,
        private val bankCityTextView: TextView,
        private val bankSiteTextView: TextView,
        private val bankPhoneTextView: TextView
) {

    companion object {

        private const val UNKNOWN_VALUE = "?"

    }

    private val context: Context
        get() = infoMainView.context

    //CardView to show
    var cardInfo: CardInfo? = null
        set(value) {
            if (field != value) {
                field = value
                onCardInfoChanged(cardInfo)
            }
        }

    init {
        onCardInfoChanged(cardInfo)
    }

    private fun onCardInfoChanged(cardInfo: CardInfo?) {
        if (cardInfo == null) {
            infoMainView.visibility = View.GONE
            return
        }

        infoMainView.visibility = View.VISIBLE

        typeTextView.text = cardInfo.type.title(context)
        schemeTextView.text = cardInfo.scheme.title(context)
        countryNameTextView.text = cardInfo.country.name
        countryFlagTextView.text = cardInfo.country.emoji
        bankNameTextView.text = cardInfo.bank.name
        bankCityTextView.text = cardInfo.bank.city ?: UNKNOWN_VALUE
        bankSiteTextView.text = cardInfo.bank.url ?: UNKNOWN_VALUE
        bankPhoneTextView.text = cardInfo.bank.phone ?: UNKNOWN_VALUE
    }

}