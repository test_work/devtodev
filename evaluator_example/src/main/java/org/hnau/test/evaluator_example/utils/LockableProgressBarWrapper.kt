package org.hnau.test.evaluator_example.utils

import android.view.View
import android.widget.ProgressBar

/**
 * ProgressBar container that shows after coroutine start end hids after end of coroutine execution
 */
class LockableProgressBarWrapper(
        private val progressBar: ProgressBar
) {

    private var locksCount = 0
        set(value) {
            if (field != value) {
                field = value
                progressBar.visibility = if (value > 0) View.VISIBLE else View.GONE
            }
        }

    private fun changeLocksCount(
            delta: Int
    ) = synchronized(this) {
        locksCount += delta
    }

    private fun incLocksCount() = changeLocksCount(1)
    private fun decLocksCount() = changeLocksCount(-1)

    /**
     * Execute [coroutine] with visible ProgressBar
     */
    suspend operator fun invoke(
            coroutine: suspend () -> Unit
    ) {
        try {
            incLocksCount()
            coroutine()
        } finally {
            decLocksCount()
        }
    }

}