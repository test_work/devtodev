package org.hnau.test.evaluator_example.utils

import android.content.Context
import android.view.View
import android.widget.TextView
import org.hnau.devtodev.base.check.CardNumberCheckingException
import org.hnau.devtodev.utils.StringGetter
import org.hnau.test.evaluator_example.R


/**
 * Error text view wrapper
 */
class EvaluatingErrorContainer(
    private val errorTextView: TextView
) {

    companion object {

        private val UNKNOWN_ERROR_TEXT = StringGetter(R.string.error_info_getting)

    }

    //Throwable to show
    var error: Throwable? = null
        set(value) {
            if (field != value) {
                field = value
                onErrorChanged(value)
            }
        }

    private val context: Context
        get() = errorTextView.context

    init {
        onErrorChanged(error)
    }

    private fun onErrorChanged(error: Throwable?) {
        if (error == null) {
            errorTextView.visibility = View.GONE
            return
        }

        val text = (error as? CardNumberCheckingException)?.text ?: UNKNOWN_ERROR_TEXT
        errorTextView.visibility = View.VISIBLE
        errorTextView.text = text(context)
    }

}