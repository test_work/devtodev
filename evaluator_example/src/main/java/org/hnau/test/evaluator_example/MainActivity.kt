package org.hnau.test.evaluator_example

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ProgressBar
import org.hnau.devtodev.Evaluator
import org.hnau.test.evaluator_example.utils.CardNumberInputContainer
import org.hnau.test.evaluator_example.utils.EvaluatingCardResultContainer
import org.hnau.test.evaluator_example.utils.LockableProgressBarWrapper
import org.hnau.test.evaluator_example.utils.UIJob
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    private val uiJob: UIJob by lazy { UIJob(this) }

    private val lockableProgressBar: LockableProgressBarWrapper by lazy {
        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)
        LockableProgressBarWrapper(progressBar)
    }

    private val evaluatingCardResultContainer: EvaluatingCardResultContainer by lazy {
        EvaluatingCardResultContainer(
                errorTextView = findViewById(R.id.error_text),
                infoMainView = findViewById(R.id.info_container),
                typeTextView = findViewById(R.id.info_type),
                schemeTextView = findViewById(R.id.info_scheme),
                countryNameTextView = findViewById(R.id.info_country_name),
                countryFlagTextView = findViewById(R.id.info_country_flag),
                bankNameTextView = findViewById(R.id.info_bank_name),
                bankCityTextView = findViewById(R.id.info_bank_city),
                bankSiteTextView = findViewById(R.id.info_bank_site),
                bankPhoneTextView = findViewById(R.id.info_bank_phone)
        )
    }

    private val cardNumberInputContainer: CardNumberInputContainer by lazy {
        CardNumberInputContainer(
                cardNumberInput = findViewById(R.id.card_number_input),
                evaluateButton = findViewById(R.id.evaluate_button)
        )
    }

    private val evaluator = Evaluator(Executors.newSingleThreadExecutor())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cardNumberInputContainer.setOnEvaluateButtonClickListener(this::onCardNumberEnter)
    }

    private fun onCardNumberEnter(cardNumber: String) {
        uiJob {
            lockableProgressBar {
                handleCardNumber(cardNumber)
            }
        }
    }

    private suspend fun handleCardNumber(cardNumber: String) {
        try {
            val cardInfo = evaluator.evaluate(cardNumber)
            evaluatingCardResultContainer.onSuccess(cardInfo)
        } catch (th: Throwable) {
            evaluatingCardResultContainer.onError(th)
        }
    }


}
