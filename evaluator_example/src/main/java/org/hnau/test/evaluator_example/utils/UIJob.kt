package org.hnau.test.evaluator_example.utils

import android.arch.lifecycle.GenericLifecycleObserver
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


/**
 * Coroutines executor that execute coroutines only when [lifecycleOwner] is visible to user
 */
class UIJob(
        private val lifecycleOwner: LifecycleOwner
) : (suspend CoroutineScope.() -> Unit) -> Unit {

    companion object {

        private val IS_ACTIVE_LIFECYCLE_EVENTS =
                hashSetOf(Lifecycle.Event.ON_START, Lifecycle.Event.ON_RESUME, Lifecycle.Event.ON_PAUSE)

    }

    private var coroutineContext: CoroutineContext? = null

    private var job: Job? = null
        set(value) = synchronized(this) {
            field?.cancel()
            field = value
            coroutineContext = value?.let { it + Dispatchers.Main }
        }

    private var active = false
        set(value) {
            if (field != value) {
                field = value
                job = value.takeIf { it }?.let { SupervisorJob() }
            }
        }

    private val lifecycleObserver = GenericLifecycleObserver { _, event ->
        active = event in IS_ACTIVE_LIFECYCLE_EVENTS
    }

    init {
        lifecycleOwner.lifecycle.addObserver(lifecycleObserver)
    }

    /**
     * Execute coroutine
     */
    override fun invoke(coroutine: suspend CoroutineScope.() -> Unit) {
        coroutineContext?.let { coroutineContext ->
            GlobalScope.launch(
                    context = coroutineContext,
                    block = coroutine
            )
        }
    }

}