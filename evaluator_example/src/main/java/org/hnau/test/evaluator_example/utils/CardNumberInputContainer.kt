package org.hnau.test.evaluator_example.utils

import android.view.View
import android.widget.EditText

/**
 * Aggregator of card number inputing views
 */
class CardNumberInputContainer(
        private val cardNumberInput: EditText,
        private val evaluateButton: View
) {

    fun setOnEvaluateButtonClickListener(listener: (cardNumber: String) -> Unit) {
        evaluateButton.setOnClickListener {
            val cardNumber = cardNumberInput.text.toString()
            listener(cardNumber)
        }
    }

}